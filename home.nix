{ pkgs, ... }:

{
  home = rec {
    username = "taka";
    homeDirectory = "/home/${username}";
    stateVersion = "24.05";
  };

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  programs = {
    direnv = {
      enable = true;
      enableBashIntegration = true;
      nix-direnv.enable = true;
    };

    bash.enable = true;
  };

  home.packages = with pkgs; [
    fd
    fzf
    bat
    btop
    eza
    pingu
    ripgrep
    neofetch
    felix-fm
    delta
    gitui
    ghq
    thefuck
    zoxide
    gdb
    deno
    bun
    volta
    biome
    rye
    ruff
    go
    zig
    zls
    nil
    rustup
    stack
    cabal-install
    vscode
    keybase
    keybase-gui
    element-desktop
  ];

  programs.brave = {
    enable = true;
    commandLineArgs = ["--ozone-platform-hint=auto"];
  };
}
