# set-nixos

```consol
sudo nixos-rebuild switch --flake ".#myNixOS"
nix run home-manager -- switch --flake ".#myHome"
```

## Acknowlegements

- [NixOSで最強のLinuxデスクトップを作ろう](https://zenn.dev/asa1984/articles/nixos-is-the-best)
- [NixOS & Flakes Book](https://nixos-and-flakes.thiscute.world/)
